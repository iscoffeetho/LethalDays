# ============================================================================ #
#                                                                              #
#                                                               /   /   \      #
#   Made By IsCoffeeTho                                       /    |      \    #
#                                                            |     |       |   #
#   Makefile                                                 |      \      |   #
#                                                            |       |     |   #
#   Last Edited: 05:01PM 28/11/2023                           \      |    /    #
#                                                               \   /   /      #
#                                                                              #
# ============================================================================ #

all: dll package

dll:
	dotnet build

package:
	cp bin/Debug/netstandard2.1/LethalDays.dll mod/BepInEx/plugins/LethalDays.dll
	cd mod && zip -r LethalDays ./*
	mv mod/LethalDays.zip LethalDays.zip

clean:
	rm -rf ./obj
	rm -rf ./bin

fclean: clean
	rm -f mod/BepInEx/plugins/LethalDays.dll
	rm -f LethalDays.zip

re: fclean all

.PHONY: all clean fclean re dll package