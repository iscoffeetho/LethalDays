﻿using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;

namespace LethalDays
{
	[BepInPlugin(MOD_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
	[BepInProcess("Lethal Company.exe")]
	public class Plugin : BaseUnityPlugin
	{
		private const string MOD_GUID = "IsCoffeeTho.LethalDays";
		private readonly Harmony harmony = new Harmony(MOD_GUID);
		public static Plugin Instance;
		internal ManualLogSource mls;

		public ConfigEntry<float> CFG_timescale;

		private void Awake()
		{
			if (Instance == null)
				Instance = this;

			Instance.CFG_timescale = Config.Bind("LethalDays",
				"TimeScale", 1f,
				"How many 'In Game Seconds' should happen a second"
			);

			mls = BepInEx.Logging.Logger.CreateLogSource(MOD_GUID);

			mls.LogInfo("Plugin LethalDays is loaded! MOD_GUID: 'IsCoffeeTho.LethalDays'");

			harmony.PatchAll(typeof(LethalDays.Plugin));
		}

		[HarmonyPatch(typeof(TimeOfDay), "Update")]
		[HarmonyPostfix]
		public static void AdjustTime(TimeOfDay __instance)
		{
			__instance.globalTimeSpeedMultiplier = 0.0167f * Instance.CFG_timescale.Value;
		}
	}
}
